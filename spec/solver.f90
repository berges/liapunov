module solver
   use global
   use gramschmidt
   implicit none
   private

   public :: exponents

contains

   subroutine exponents(p)
      type(info), intent(inout) :: p

      integer :: i, j
      real(dp) :: U(p%d, p%d), l(p%d), d(p%d)

      call random_number(U)

      d(:) = 0.0_dp

      do i = 1, p%no
         do j = 1, p%ni
            call p%model
            U(:, :) = matmul(p%M, U)
         end do

         call orthonormalize(U, l)

         d(:) = d + log(l) / p%ni
         p%g(:, i) = d / i
      end do
   end subroutine exponents
end module solver
