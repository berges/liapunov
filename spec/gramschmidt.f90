module gramschmidt
   use global
   implicit none
   private

   public :: orthonormalize

contains

   subroutine orthonormalize(A, l)
      real(dp), intent(inout) :: A(:, :)
      real(dp), intent(out) :: l(:)

      integer :: i, j
      real(dp) :: t(size(A, 1))

      do i = 1, size(A, 2)
         t(:) = A(:, i)

         do j = 1, i - 1
            A(:, i) = A(:, i) - A(:, j) * dot_product(A(:, j), t)
         end do

         l(i) = sqrt(dot_product(A(:, i), A(:, i)))
         A(:, i) = A(:, i) / l(i)
      end do
   end subroutine orthonormalize
end module gramschmidt
