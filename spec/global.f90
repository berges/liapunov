module global
   implicit none
   private

   public :: info, dp

   integer, parameter :: dp = selected_real_kind(14, 300)

   type info
      character(:), allocatable :: name

      integer :: no ! Number of outer iterations
      integer :: ni ! Number of inner iterations

      logical :: all ! Store intermediate values?

      real(dp) :: E ! Energy

      procedure(f), pass(x), pointer :: model ! Disorder model

      real(dp) :: V ! Disorder strength
      real(dp) :: c ! Disorder concentration

      integer :: b ! Dimension of H
      integer :: d ! Dimension of M

      real(dp), allocatable :: H(:, :) ! Hamilton operator of cross-section
      real(dp), allocatable :: M(:, :) ! Transfer matrix

      real(dp), allocatable :: g(:, :) ! Liapunov exponents
   end type info

   abstract interface
      subroutine f(x)
         import info
         class(info), intent(inout) :: x
      end subroutine f
   end interface
end module global
