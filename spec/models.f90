module models
   use global
   implicit none
   private

   public :: anderson, delta

   integer :: i

contains

   subroutine anderson(p)
      class(info), intent(inout) :: p

      do i = 1, p%b
         call random_number(p%M(i, i))
         p%M(i, i) = p%E - p%V * (p%M(i, i) - 0.5_dp)
      end do
   end subroutine anderson

   subroutine delta(p)
      class(info), intent(inout) :: p

      do i = 1, p%b
         call random_number(p%M(i, i))
         p%M(i, i) = p%E - merge(p%V, 0.0_dp, p%M(i, i) < p%c)
      end do
   end subroutine delta
end module models
