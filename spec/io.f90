module io
   use global
   use models
   use paths
   implicit none
   private

   public :: load, store

   character(20) :: temp
   integer, parameter :: unit = 11

contains

   subroutine load(file, p)
      character(*), intent(in) :: file
      type(info), intent(out) :: p

      integer :: i

      p%name = stem(file)

      open(unit, file=file, action='read', status='old')

      read (unit, *) p%no
      read (unit, *) p%ni

      read (unit, *) p%all

      read (unit, *) p%E

      read (unit, *) temp
      read (unit, *) p%V
      read (unit, *) p%c

      select case (temp)
         case ('Anderson')
            p%model => anderson

         case ('Delta')
            p%V = p%V * sqrt(1.0_dp - (p%E / 2) ** 2) / acos(p%E / 2)
            p%model => delta

         case default
            stop 'Unknown disorder model'
      end select

      read (unit, *) p%b

      allocate(p%H(p%b, p%b))

      do i = 1, p%b
         read (unit, *) p%H(i, :i)
         p%H(:i - 1, i) = p%H(i, :i - 1)
      end do

      close(unit)

      p%d = 2 * p%b

      allocate(p%M(p%d, p%d))

      p%M(:, :) = 0.0_dp
      p%M(:p%b, :p%b) = -p%H

      do i = 1, p%b
         p%M(p%b + i, i) = +1.0_dp
         p%M(i, p%b + i) = -1.0_dp
      end do

      allocate(p%g(p%d, p%no))
   end subroutine load

   subroutine store(p)
      type(info), intent(in) :: p

      open(unit, file=p%name // '.out', action='write', status='replace')

      if (p%all) then
         write (temp, '(A, I0, A)') '(', p%d, '(ES9.3E1, :, X))'
         write (unit, temp) p%g
      else
         write (unit, '(ES21.13E3)') p%g(:, p%no)
      end if

      close(unit)
   end subroutine store
end module io
