\documentclass[a4paper, 10pt]{article}

\usepackage[margin=3cm]{geometry}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage[math]{iwona}
\usepackage[T1]{fontenc}

\let\varhbar\hbar

\usepackage{amsmath, amssymb, bbold, slantsc, graphicx, listings, color}

\usepackage[labelfont=bf, font=small]{caption}

\setcounter{topnumber}{2}
\setcounter{bottomnumber}{2}

\def \textfraction {0.1}
\def \topfraction {0.9}
\def \bottomfraction {0.9}
\def \floatpagefraction {0.7}

\pdfsuppresswarningpagegroup=1

\definecolor{grey}{rgb}{0.5, 0.5, 0.5}
\definecolor{dark}{rgb}{0.0, 0.0, 0.5}

\lstdefinelanguage{f90}{
	keywords = {abstract, allocatable, allocate, call, case, character, class, close, contains, default, do, else, end, function, if, implicit, integer, intent, interface, logical, module, open, optional, parameter, pass, pointer, private, procedure, program, public, read, real, save, select, stop, subroutine, then, type, use, write},
	comment = [l]{!},
	string = [b]',
	morestring = [b]",
	sensitive = false}

\lstdefinelanguage{py}{
    keywords = {and, as, assert, break, class, continue, def, del, elif, else, except, exec, finally, for, from, global, if, import, in, is, lambda, not, or, pass, print, raise, return, try, while, with, yield}
    comment = [l]{\#},
    string = [b]',
    morestring = [b]",
    sensitive = true}

\lstset{
  	texcl = true,
	escapebegin = \normalfont \,,
	escapeend = \endgraf,
	commentstyle = \color{black},
	%
	basicstyle = \color{dark} \tt \small,
	keywordstyle = \color{blue},
	identifierstyle = \color{black},
	%
	numbers = left,
	numbersep = 4mm,
	numberstyle = \color{grey} \tiny,
	%
	showstringspaces = false,
	%
	aboveskip = 4mm,
	belowskip = 4mm}

\let\op\mathrm
\let\vec\boldsymbol

\let\hbar\varhbar
\let\epsilon\varepsilon

\def\conj{\sp\ast}

\def\sub#1{\sb{\text{#1}}}

\def\D{\mathrm d}
\def\E{\mathrm e}
\def\I{\mathrm i}

\def\parens#1{\left(#1\right)}
\def\brackets#1{\left[#1\right]}
\def\braces#1{\left\lbrace#1\right\rbrace}

\def\bra#1{\langle#1|}
\def\ket#1{|#1\rangle}
\def\bracket#1#2{\langle#1|#2\rangle}

\def\key#1 {\hspace{0pc}\llap{\textbf{#1}}}
\def\info#1{\hspace{1pc}\rlap{#1}}

\def\|{\rlap|\hspace{2pt}|}

\usepackage{hyperref}

\begin{document}
    \begin{center}
        \large Project report

        \vspace{5mm}

        \huge \textsc{Liapunov} exponents of nanowires

        \vspace{1cm}

        \normalsize \key{Student} \info{Jan Berges}

        \vspace{0.5em}

        \key{Seminar} \info{\emph{Electronic Transport at Nanoscale}}

        \info{Dr. Gabriele Penazzi}

        \info{BCCMS, University of Bremen}

        \vspace{0.5em}

        \key{Date of examination} \info{September 7, 2015}

        \vspace{1cm}
    \end{center}

    \tableofcontents

    \section{Introduction}

    The purpose of this report is to demonstrate the principles of the transfer matrix method applied to quasi one-dimensional systems or \emph{nanowires}. In this context, the so-called \textsc{Liapunov} exponents are of special interest, not least because they give information about the localization length of the system.

    Past a review of the necessary theoretical background, an algorithm for the numerical approximation of the \textsc{Liapunov} exponents is presented on the basis of a concrete implementation. Finally, some physical results for different parameter sets are shown.

    If not stated differently, subject-specific facts are taken from Dr. Gabriele Penazzi's lecture notes for the seminar \emph{Electronic Transport at Nanoscale} \cite{Penazzi}.

    \section{The transfer matrix formalism}

    \subsection{Plane waves representation}

    The concept and usage of transfer matrices is introduced using the example of the one-dimensional delta potential $V(x) = \alpha \delta(x)$. The corresponding stationary \textsc{Schrödinger} equation reads
    %
    \begin{align*}
        -\frac{\hbar^2}{2 m} \frac{\partial^2 \psi}{\partial x^2} + \alpha \delta(x) \psi(x) = E \psi(x).
    \end{align*}
    %
    For $x \neq 0$, i.e. before and after the barrier, this reduces to
    %
    \begin{align*}
        \frac{\partial^2 \psi}{\partial x^2} = -\frac{2 m E}{\hbar^2} \psi(x).
    \end{align*}
    %
    It is solved by plane waves or, more precisely, linear combinations of
    %
    \begin{align*}
        \psi(x) \propto \E^{\pm \I k x}, \quad \text{where} \quad k = \frac {\sqrt{2 m E}} \hbar.
    \end{align*}
    %
    The ansatz, which is chosen here, is continuous at $x = 0$ and reads
    %
    \begin{align}
        \psi(x) = \begin{cases} \psi_A(x) = A^+ \E^{\I k x} + A^- \E^{-\I k x} & x \leq 0, \\ \psi_B(x) = B^+ \E^{\I k x} + B^- \E^{-\I k x} & x \geq 0. \end{cases} \label{ansatz}
    \end{align}
    %
    The full \textsc{Schrödinger} equation is now integrated over the infinitesimal interval $[-\epsilon, +\epsilon]$, thus
    %
    \begin{align*}
        -\frac{\hbar^2}{2 m} \int \limits_{-\epsilon}^{+\epsilon} \D x \, \frac{\partial^2 \psi}{\partial x^2} + \alpha \int \limits_{-\epsilon}^{+\epsilon} \D x \, \delta(x) \psi(x) = -\frac{\hbar^2}{2 m} \brackets{\frac{\partial \psi}{\partial x}}_{-\epsilon}^{+\epsilon} + \alpha \psi(0) = E \int \limits_{-\epsilon}^{+\epsilon} \D x \, \psi(x) = 0.
    \end{align*}
    %
    Combining this with eq.~\ref{ansatz} yields
    %
    \begin{align*}
        \brackets{\frac{\partial \psi}{\partial x}}_{-\epsilon}^{+\epsilon} = \I k (B^+ - B^- - A^+ + A^-) = \frac{2 m \alpha}{\hbar^2} \psi(0) \quad \text{with} \quad \psi(0) &=  A^+ + A^- = B^+ + B^-.
    \end{align*}
    %
    The amplitude coefficients $B^+$ and $B^-$ are thus defined by the linear system of equations
    %
    \begin{align*}
        B^+ + B^- &= A^+ + A^- \tag{I} \label{I} \\
        \text{and} \quad B^+ - B^- &= \frac{2 m \alpha}{\I k \hbar^2} (A^+ + A^-) + A^+ - A^- \tag{II} \label{II}
    \end{align*}
    %
    the solutions of which are
    %
    \begin{align*}
        B^\pm = \frac {\mathrm{(\ref{I})} \pm \mathrm{(\ref{II})}} 2 = A^\pm \mp \I \gamma (A^+ + A^-), \quad \text{where} \quad \gamma = \frac{m \alpha}{k \hbar^2} = \frac \alpha \hbar \sqrt{\frac{m}{2 E}}.
    \end{align*}
    %
    Using matrix multiplication, this can be equivalently formulated as
    %
    \begin{align}
        \begin{bmatrix} B^+ \\ B^- \end{bmatrix} = \underbrace{\begin{bmatrix} 1 - \I \gamma & -\I \gamma \\ \I \gamma & 1 + \I \gamma \end{bmatrix}}_D \begin{bmatrix} A^+ \\ A^- \end{bmatrix}. \label{D}
    \end{align}
    %
    Here, the \emph{transfer matrix} $D$ links the wave function amplitudes on the different sides of the barrier for the two orientations of propagation. In analogy, for undisturbed propagation through $V(x) = 0$ across a distance $a$ one finds, the definition of $\psi_A$ being untouched,
    %
    \begin{align}
        \psi_B(x) = B^+ \E^{\I k (x + a)} + B^- \E^{-\I k (x + a)} \quad \text{or} \quad \begin{bmatrix} B^+ \\ B^- \end{bmatrix} = \underbrace{\begin{bmatrix} \E^{\I k a} & 0 \\ 0 & \E^{-\I k a} \end{bmatrix}}_P \begin{bmatrix} A^+ \\ A^- \end{bmatrix}. \label{P}
    \end{align}
    %
    One mayor advantage of the transfer matrix formalism is the possibility of describing composite barriers simply as a product of the corresponding matrices. E.g. for two delta potentials, separated by a distance $a$, is reads $D_2 = D P D$.

    \subsection{Tight-binding model}

    Consider a one-dimensional lattice with orthonormal states $\ket i$ localized at the lattice points $i a$, where $a$ is the lattice constant and $i \in \mathbb Z$. With a coupling $t$ between neighboring sites and an on-site energy $\epsilon$ the tight-binding \textsc{Hamilton} operator reads
    %
    \begin{align*}
        \op H = \sum_i \ket i \brackets{\bra{i - 1} t + \bra i \epsilon + \bra{i + 1} t}.
    \end{align*}
    %
    The total wave function can be expressed in terms of the complete set of localized states, i.e.
    %
    \begin{align*}
        \ket \psi = \sum_i \ket i \bracket i \psi = \sum_i c_i \ket i.
    \end{align*}
    %
    The eigenvalue equation $(\op H - E) \ket \psi = 0$ holds, so does
    %
    \begin{align*}
        \bra i (\op H - E) \ket \psi &= \bra i \sum_j \ket j \brackets{\bra{j - 1} t + \bra j (\epsilon - E) + \bra{j + 1} t} \ket \psi \\
        &= \sum_j \bracket i j \brackets{t c_{j - 1} + (\epsilon - E) c_j + t c_{j + 1}} = t c_{i - 1} + (\epsilon - E) c_i + t c_{i + 1} = 0 \\
        \text{or equivalently} \quad c_{i + 1} &= \frac {E - \epsilon} t c_i - c_{i - 1}.
    \end{align*}
    %
    In combination with the identity $c_i = c_i$ this can be rewritten using a transfer matrix $\mathcal P$, thus
    %
    \begin{align}
        \begin{bmatrix} c_{i + 1} \\ c_i \end{bmatrix} = \underbrace{\begin{bmatrix} \frac {E - \epsilon} t & -1 \\ 1 & 0 \end{bmatrix}}_{\mathcal P} \begin{bmatrix} c_i \\ c_{i - 1} \end{bmatrix}. \label{P'}
    \end{align}
    %
    This formula is equivalent to the discretized \textsc{Schrödinger} equation
    %
    \begin{align}
        -\frac{\hbar^2}{2 m} \frac{\psi_{i + 1} - 2 \psi_i + \psi_{i - 1}}{a^2} + V_i \psi_i = E \psi_i \quad \text{implying} \quad \epsilon = \frac{\hbar^2}{m a^2} + V_i \quad \text{and} \quad t = -\frac{\hbar^2}{2 m a^2}. \label{discretized}
    \end{align}

    \subsubsection{Quasi one-dimensional systems}

    In case of a nanowire with finite lateral extent consisting of $N$ parallel strands $\ket n$, some of which are coupled at the lattice points, one has to work in a basis of product states $\ket{i n} = \ket i \ket n$, thus
    %
    \begin{align*}
        \op H = \sum_{i n} \ket{i n} \brackets{\bra{i - 1 \ n} t + \bra{i + 1 \ n} t + \sum_m \bra{i m} t_{n m}}, \quad \text{where} \quad t_{n n} = \epsilon.
    \end{align*}
    %
    Again, one can write $\ket \psi = \sum_{i n} \ket{i n} \bracket {i n} \psi = \sum_{i n} c_i^n \psi$ as well as
    %
    \begin{align*}
        0 &= \bra{i n} (\op H - E) \ket \psi = t c_{i - 1}^n + t c_{i + 1}^n + \sum_m (t_{n m} - \delta_{n m} E) c_i^{m} \\
        \text{or} \quad \vec c_{i + 1} &= \frac {E \mathbb 1 - H'} t \vec c_i - \vec c_{i - 1}, \quad \text{where} \quad \vec c_i = \begin{bmatrix} c_i^1 \\ \vdots \\ c_i^N \end{bmatrix} \quad \text{and} \quad H' = \begin{bmatrix} t_{1 1} & \dots & t_{1 N} \\ \vdots & \ddots & \\ t_{N 1} & & t_{N N} \end{bmatrix}
    \end{align*}
    %
    is the \textsc{Hamilton} matrix of the cross section. The $2 N \times 2 N$ transfer matrix fulfills
    %
    \begin{align*}
        \begin{bmatrix} \vec c_{i + 1} \\ \vec c_i \end{bmatrix} = \begin{bmatrix} \frac {E \mathbb 1 - H'} t & -\mathbb 1 \\ \mathbb 1 & \mathbb 0 \end{bmatrix} \begin{bmatrix} \vec c_i \\ \vec c_{i - 1} \end{bmatrix}.
    \end{align*}

    \subsection{Transformation between both representations}
    \label{transformation}

    It can be shown that for vanishing on-site energies $\epsilon$ the relation between eq.~\ref{P} and \ref{P'} is given by
    %
    \begin{align*}
        \mathcal P = \underbrace{\begin{bmatrix} 1 & 1 \\ \E^{-\I k a} & \E^{\I k a} \end{bmatrix}}_Q \underbrace{\begin{bmatrix} \E^{\I k a} & 0 \\ 0 & \E^{-\I k a} \end{bmatrix}}_P \underbrace{\begin{bmatrix} \E^{\I k a} & -1 \\ -\E^{-\I k a} & 1 \end{bmatrix} \frac 1 {\E^{\I k a} - \E^{-\I k a}}}_{Q^{-1}} = \begin{bmatrix} 2 \cos(k a) & -1 \\ 1 & 0 \end{bmatrix} = \begin{bmatrix} \frac E t & -1 \\ 1 & 0 \end{bmatrix},
    \end{align*}
    %
    where $E(k) = 2 t \cos(k a)$ is the well-known dispersion of the linear tight-binding chain. Equivalently, for a combination $P D$ of free propagation and delta barrier according to eq.~\ref{D} one finds,\footnote{This solution, without the substitution for $k$, appears for instance in \cite{Izrailev}.} using eq.~\ref{discretized},
    %
    \begin{align}
        \mathcal{P D} = Q P D Q^{-1} &= \begin{bmatrix} 2 \cos(k a) + 2 \gamma(k) \sin(k a) & -1 \\ 1 & 0 \end{bmatrix} = \begin{bmatrix} \frac {E - V f(E)} t & -1 \\ 1 & 0 \end{bmatrix}, \notag \\
        \text{where} \quad f(E) &= \frac{\sin(k a)}{k a} = \frac{\sqrt{1 - \parens{\frac E {2 t}}^2}}{\arccos \frac E {2 t}} \in [0, 1] \quad \text{and} \quad V = \frac \alpha a. \label{PD}
    \end{align}

    \section{The \textsc{Liapunov} exponent}

    Let $M_n = \mathcal M_n \dots \mathcal M_1$ be the product of $n$ random matrices $\mathcal M_i$ -- such as the transfer matrices $\mathcal P$ with varying on-site energies -- for which the \textsc{Osedelets} theorem holds, i.e. let
    %
    \begin{gather*}
        \lim_{n \rightarrow \infty} M_n^{1 / n}
    \end{gather*}
    %
    be existent and unique. Consequently, the so-called \emph{\textsc{Ljapunov} exponent}
    %
    \begin{gather*}
        \gamma(\vec u) = \lim_{n \rightarrow \infty} \frac 1 n \log \frac{\| M_n \vec u \|}{\| \vec u \|}
    \end{gather*}
    %
    exists as well. For $\vec u$ an eigenvector of $M_n$ belonging to the asymptotic eigenvalue $\lambda^n$ one finds
    %
    \begin{gather*}
        \gamma(\vec u) = \lim_{n \rightarrow \infty} \frac 1 n \log \frac{\| \lambda^n \vec u \|}{\| \vec u \|} = \lim_{n \rightarrow \infty} \frac 1 n \log \lambda^n = \lim_{n \rightarrow \infty} \log \lambda = \log \lambda.
    \end{gather*}
    %
    But $\vec u$ does not necessarily have to be an eigenvector of $M_n$ since $M_n \vec u$ will automatically converge towards such with growing $n$.

    For the powers of a single non-random operator $\op A$ this can be illustrated as follows: Let $\ket i$ be the normalized eigenvectors of $\op A$ belonging to the eigenvalues $a_i$ which differ pairwise in magnitude. For great $n$ the expression
    %
    \begin{align*}
        \op A^n \ket x = \op A^n \sum_i \ket i \bracket i x = \sum_i a_i^n \ket i \bracket i x.
    \end{align*}
    %
    will converge towards the eigenvector $\ket j$ belonging to the eigenvalue with the highest magnitude for which $\bracket j x \neq 0$.

    In the tight-binding representation, each vector $[c_i \ c_{i - 1}]$ corresponds to a wave function pointing in a certain direction. In consequence, $[c_{i - 1} \ c_i]$ describes the spatially inverted wave function with opposite direction. Let the initial $[c_0 \ c_{-1}]$ be an eigenvector of $M_n$, belonging to the eigenvalue $\lambda_1^n = \E^{\gamma_1 n}$. Hence
    %
    \begin{align*}
        \begin{bmatrix} c_n \\ c_{n - 1} \end{bmatrix} = M_n \begin{bmatrix} c_0 \\ c_{-1} \end{bmatrix} = \lambda^n \begin{bmatrix} c_0 \\ c_{-1} \end{bmatrix}.
    \end{align*}
    %
    For $\gamma_1 = -\gamma < 0$, this implies an exponential decay of the amplitude, i.e.
    %
    \begin{align*}
        c_n = \E^{-\gamma n} c_0.
    \end{align*}
    %
    For mentioned symmetry reason, $[c_{-1} \ c_0]$ must be an eigenvector as well, namely the one belonging to $\lambda_2^n = \E^{\gamma_2 n} = \E^{\gamma n}$. Here, one finds the inverted relation $c_n = \E^{\gamma n} c_0$. In both cases the amplitude decays alongside the direction of propagation. The transmittance $t$ and the transmission $T$ for the passage through a continuous distance $l = n a$ are
    %
    \begin{align*}
        t(l) \propto \E^{-\gamma n} = \E^{-\gamma l / a} \quad \text{and} \quad T = t t \conj \propto \E^{-2 \gamma l / a} = \E^{-l / l \sub{loc.}},
    \end{align*}
    %
    respectively, where $l \sub{loc.}$ is the \emph{localization length}. Comparison shows that
    %
    \begin{align}
        l \sub{loc.} = \frac a {2 \gamma}. \label{loclen}
    \end{align}
    %
    For quasi one-dimensional systems with multiple pairs of \textsc{Liapunov} exponents one defines the maximum localization length with respect to the exponent of least magnitude.

    \subsection{Numerical approximation}
    \label{algorithm}

    An arbitrary system state can be represented as a superposition of eigenstates of $M_n$, thus as a linear combination of exponentially growing and decaying components. Since the addition of floating point numbers which differ dramatically in magnitude is accompanied with an enormous loss of accuracy, one has to resign from simple multiplication of random matrices and diagonalization of their product in order to calculate the \textsc{Liapunov} exponents, but rather use an iterative algorithm:
    %
    \begin{enumerate}
        \item Apply only a few number, $m$ say, of random transfer matrices to the vectors $\vec u_1 \hdots \vec u_{2 N}$, starting from an arbitrary guess.
        \item Orthogonalize $\vec u_1 \hdots \vec u_{2 N}$ using the \textsc{Gram-Schmidt} algorithm.
        \item Modify the temporary values $d_1 \dots d_{2 N}$, initially zero, according to
        %
        \begin{align*}
            d_k \leftarrow d_k + \frac 1 m \log \| \vec u_k \|.
        \end{align*}
        %
        Note that, if $\vec u_k$ were always the appropriate eigenvector, for the $n$-th iteration one would find
        %
        \begin{align*}
            d_k = n \cdot \frac 1 m \cdot \log \lambda_i^m, \quad \text{thus} \quad \gamma_k = \frac {d_k} n.
        \end{align*}
        %
        \item Normalize each $\vec u_k$ and repeat.
    \end{enumerate}

    \section{Disorder models}

    \subsection{\textsc{Anderson} disorder model}

    This \emph{amplitude disorder}\footnote{The nomenclature is borrowed from \cite{Izrailev}.} model assumes a random perturbation energy for each site individually. In case of $2 \times 2$ transfer matrices one finds, as a variation of $\mathcal P$ from eq.~\ref{P'} for vanishing on-site energies,
    %
    \begin{align*}
        \mathcal M_i = \begin{bmatrix} \frac {E - \delta_i} t  & -1 \\ 1 & 0 \end{bmatrix}, \quad \text{where} \quad \delta_i \in \brackets{-\frac V 2, \frac V 2}.
    \end{align*}

    \subsection{Random distribution of delta scatterers}

    Now a \emph{positional disorder} is considered, i.e. only a random subset of sites features a perturbation energy, which is always the same. The corresponding transfer matrices are the ones presented in section~\ref{transformation}, more precisely
    %
    \begin{align*}
        \mathcal M_i = \begin{cases} \mathcal{P D} & \text{if the $i$-th site is perturbed,} \\ \mathcal P & \text{otherwise.} \end{cases}
    \end{align*}
    %
    Besides $V$, one must also choose an impurity concentration $c$. The above assignment does not hold for higher dimensions since the diagonal elements of interest have to be treated independently.

    \section{Implementation}

    The different tasks are distributed as follows: While the computationally intensive calculation of the \textsc{Liapunov} exponents is done by a \emph{Fortran} program, a \emph{Python} wrapper script assumes the application to different sets of input parameters as well as the graphical output.

    \subsection{Main program}

    The source code of the main program defines the workflow only superficially: Having initialized the pseudo-random number generator, it processes the list of input files passed via the command line and creates an output file with \textsc{Liapunov} exponents each.
    %
    \lstinputlisting[language=f90]{../liapunov.f90}

    \subsection{Specific modules}

    The following modules are particularly related with the matter of interest.

    \subsubsection{global.f90}

    Herein, the accuracy of floating point numbers and the data type \lstinline{info} for the information to be passed around are defined for global availability. The variable \lstinline{parameters%model} will point to the subroutine which simulates the chosen kind of impurities. Therefore the corresponding interface has to be known in advance.
    %
    \lstinputlisting[language=f90]{../spec/global.f90}

    \subsubsection{io.f90}

    This module provides one subroutine to read and preprocess the information from the input files and another to write the calculated \textsc{Liapunov} exponents into the appropriate output files. As required, all intermediate values or just the final ones are stored.
    %
    \lstinputlisting[language=f90]{../spec/io.f90}

    \subsubsection{gramschmidt.f90}

    The following subroutine orthogonalizes and normalizes the columns of the matrix \lstinline{A}. The lengths they had in between both steps are provided in \lstinline{l}.
    %
    \lstinputlisting[language=f90]{../spec/gramschmidt.f90}

    \subsubsection{solver.f90}

    The \textsc{Liapunov} exponents are approximated iteratively as described in section~\ref{algorithm}.
    %
    \lstinputlisting[language=f90]{../spec/solver.f90}

    \subsubsection{models.f90}

    These subroutines simulate impurities, randomizing the diagonal of the upper left block of the transfer matrix at each iteration. Any information about on-site energies that was possibly defined in the cross-section \textsc{Hamilton} operator is destroyed.
    %
    \lstinputlisting[language=f90]{../spec/models.f90}

    \subsection{Miscellaneous modules}

    These modules are applicable to a wider field than the ones described above. In particular, they have no dependencies themselves.

    \subsubsection{command.f90}

    The ensuing function returns a string with the \lstinline{n}-th command line argument passed to the program. The physical lengths matches the logical one exactly and is theoretically unlimited.
    %
    \lstinputlisting[language=f90]{../misc/command.f90}

    \subsubsection{paths.f90}

    This module provides a function that determines the stem of a given file name oder path, i.e. everything but a possible suffix.
    %
    \lstinputlisting[language=f90]{../misc/paths.f90}

    \subsubsection{random.f90}

    The following does nothing but initialize the pseudo-random number generator.
    %
    \lstinputlisting[language=f90]{../misc/random.f90}

    \subsection{Example of application}

    \subsubsection{figures.py}

    This is the wrapper script by means of which some exemplary physical systems are studied. Repeatedly, it creates a temporary input file using \emph{template.in}, runs the main program \emph{liapunov} and reads the resulting output file. Past processing the data some plots are created.

    \lstinputlisting[language=py]{figures.py}

    \subsubsection{template.in}

    This input file template features some voids to be filled by \emph{figures.py}.
    %
    \lstinputlisting{template.in}

    \section{Evaluation of the results}

    If not stated differently, the data which is presented in this section results from 100\,000 \textsc{Gram-Schmidt} orthogonalizations between each of which 50 transfer matrices were applied to the state vectors.

    \subsection{\textsc{Anderson} disorder model}

    In fig.~\ref{Agn} it is shown how the two \textsc{Liapunov} exponents for the one-dimensional \textsc{Anderson} model approximate their limites of equal magnitude but opposite sign, as expected.

    According to eq.~\ref{loclen}, from the final values one can calculate the localization length $l \sub{loc.}$ which is studied for different energies $E$ and disorder strengths $V$.

    Unsurprisingly, fig.~\ref{AlEV} demonstrates that $l \sub{loc.}$ gets shorter for stronger disorder potentials. Additionally, fig.~\ref{AlVE} reveals that it vanishes completely for energies $|E| > 2 t$, which is a consequence of the dispersion relation featuring no bands beyond this domain.

    \subsection{Delta scatterers}

    From now on it is assumed that $E = 0$ which implies $f(E) = 2 / \pi$, considering eq.~\ref{PD}.

    $l \sub{loc.}$ is determined for different potential strengths $V$ but also for different impurity concentrations $c$. Again, the results, presented in fig.~\ref{dlcV} and \ref{dlVc}, exhibit a decay with growing disorder, but at least for the $V$-dependency one can formulate a more precise description:

    According to \textsc{Fermi}'s golden rule, the transition rate for elastic scattering reads
    %
    \begin{gather*}
        s_{\vec k \rightarrow \vec k'} = \frac {2 \pi} \hbar |\bra{\vec k'} V \ket{\vec k}|^2 \delta(\epsilon_{\vec k'} - \epsilon_{\vec k}) = s_{\vec k' \rightarrow \vec k}.
    \end{gather*}
    %
    In case of a scattering potential $V(\vec r) = \alpha \sum_{\vec R} \delta(\vec r - \vec R)$ and plane waves $\bracket{\vec r}{\vec k} = \frac 1 {\sqrt V} \E^{\I \vec k \vec r}$ one finds
    %
    \begin{align*}
        |\bra{\vec k'} V \ket{\vec k}|^2 &= \iint \bracket{\vec k'}{\vec r} \, \D^3 r \, \bra{\vec r} V \ket{\vec k} \bracket{\vec k}{\vec r'} \, \D^3 r' \, \bra{\vec r'} V \ket{\vec k} \\
        &= \frac{\alpha^2}{V^2} \sum_{\vec R \vec R'} \iint \D^3 r \, \D^3 r' \, \E^{\I (\vec k - \vec k') (\vec r - \vec r')} \delta(\vec r - \vec R) \delta(\vec r' - \vec R') \\
        &= \frac{\alpha^2}{V^2} \sum_{\vec R \vec R'} \E^{\I (\vec k - \vec k') (\vec R - \vec R')} \approx \frac {\alpha^2 c} V
    \end{align*}
    %
    for the mean configuration.\footnote{See \cite[p. 273 ff.]{Czycholl}} The linearized \textsc{Boltzmann} equation in relaxation time approximation is
    %
    \begin{gather*}
        \vec E \vec v e \frac{\partial f_0}{\partial \epsilon_{\vec k}} = \frac V {(2 \pi)^3} \int \D^3 k' \, s_{\vec k \rightarrow \vec k'} [f(\vec k') - f(\vec k)] = -\frac {f_1} \tau,
    \end{gather*}
    %
    where $\vec E$ is the driving electric field, $\vec v$ the group velocity, $e$ the charge, $f_1$ the deviation from the equilibrium density $f_0$ and $\tau$ the relaxation time. Solving it, one can conclude from comparison that
    %
    \begin{gather*}
        \frac 1 {\tau(\epsilon)} \propto \rho(\epsilon_{\vec k}) \alpha^2
    \end{gather*}
    %
    with the density of states $\rho$. The localization length is proportional to the mean free path $v \tau$, thus
    %
    \begin{gather*}
        l \sub{loc.} \propto \alpha^{-2}.
    \end{gather*}

    \subsubsection{Generalization to higher dimensions}

    In addition to the single strand, the following $2 \times 2$ and $3 \times 3$ cross-sections are considered:

    \begin{verbatim}
                                         1 - 2 - 3
                           1 - 2         |   |   |
                           |   |         4 - 5 - 6
                           3 - 4         |   |   |
                                         7 - 8 - 9
    \end{verbatim}
    %
    With the lines representing hopping coupling between neighboring sites and vanishing on-site energies, the corresponding tight-binding \textsc{Hamilton} matrices read
    %
    \begin{align*}
        H_{2 \times 2} = \begin{bmatrix}
            0 & t & t & 0 \\
            t & 0 & 0 & t \\
            t & 0 & 0 & t \\
            0 & t & t & 0
        \end{bmatrix}
        \quad \text{and} \quad
        H_{3 \times 3} = \begin{bmatrix}
            0 & t & 0 & t & 0 & 0 & 0 & 0 & 0 \\
            t & 0 & t & 0 & t & 0 & 0 & 0 & 0 \\
            0 & t & 0 & 0 & 0 & t & 0 & 0 & 0 \\
            t & 0 & 0 & 0 & t & 0 & t & 0 & 0 \\
            0 & t & 0 & t & 0 & t & 0 & t & 0 \\
            0 & 0 & t & 0 & t & 0 & 0 & 0 & t \\
            0 & 0 & 0 & t & 0 & 0 & 0 & t & 0 \\
            0 & 0 & 0 & 0 & t & 0 & t & 0 & t \\
            0 & 0 & 0 & 0 & 0 & t & 0 & t & 0
        \end{bmatrix}.
    \end{align*}
    %
    To guarantee convergence towards the right \textsc{Liapunov} exponents it proves necessary to reduce the number of iteration between successive applications of the \textsc{Gram-Schmidt} algorithm, which from now on amounts to ten.

    The intermediate \textsc{Liapunov} exponents are shown in fig.~\ref{dg2n} and \ref{dg3n}. One has to bear in mind that only the one with the smallest magnitude -- in this case close to zero -- determines the localization length as defined here.

    For the three different cross-sections the latter is displayed in fig.~\ref{dldc} and \ref{dldV} dependent on $V$ and $c$. It becomes clear, that in there is no strict hierarchy among the different designs.

    \addcontentsline{toc}{section}{References}
    %
    \begin{thebibliography}{\hspace{1cm}}
        \bibitem[Pen]{Penazzi} \textsc{G. Penazzi:} \emph{Lecture Notes: Electronic Transport at Nanoscale.} University of Bremen, 2015
        \bibitem[Izr]{Izrailev} \textsc{F. M. Izrailev, A. A. Krokhin, N. M. Makarov:} \emph{Anomalous Localization in Low-Dimensional Systems with Correlated Disorder.} Physics Reports 512, 2012
        \bibitem[Czy]{Czycholl} \textsc{G. Czycholl:} \emph{Theoretische Festkörperphysik. Von den klassischen Modellen zu modernen Forschungsthemen.} Springer-Verlag, Berlin/Heidelberg, 2008
    \end{thebibliography}

    \clearpage

    \begin{figure}
        \includegraphics[width=\textwidth]{fig/Agn.pdf}
        \caption{Intermediate \textsc{Liapunov} exponents for the one-dimensional \textsc{Anderson} model with $E = 0.2 t$ and $V = t$}
        \label{Agn}
    \end{figure}

    \begin{figure}
        \includegraphics[width=\textwidth]{fig/AlEV.pdf}
        \caption{\textsc{Anderson} localization length as a function of $V$ for different $E$}
        \label{AlEV}
    \end{figure}

    \begin{figure}
        \includegraphics[width=\textwidth]{fig/AlVE.pdf}
        \caption{\textsc{Anderson} localization length as a function of $E$ for different $V$}
        \label{AlVE}
    \end{figure}

    \begin{figure}
        \includegraphics[width=\textwidth]{fig/dlcV.pdf}
        \caption{Localization length as a function of $V$ for different $c$}
        \label{dlcV}
    \end{figure}

    \begin{figure}
        \includegraphics[width=\textwidth]{fig/dlVc.pdf}
        \caption{Localization length as a function of $c$ for different $V$}
        \label{dlVc}
    \end{figure}

    \begin{figure}
        \includegraphics[width=\textwidth]{fig/dg2n.pdf}
        \caption{Intermediate \textsc{Liapunov} exponents for a system with $2 \times 2$ cross-section, $c = 0.1$ and $V = 3 t$}
        \label{dg2n}
    \end{figure}

    \begin{figure}
        \includegraphics[width=\textwidth]{fig/dg3n.pdf}
        \caption{Intermediate \textsc{Liapunov} exponents for a system with $3 \times 3$ cross-section, $c = 0.1$ and $V = 3 t$}
        \label{dg3n}
    \end{figure}

    \begin{figure}
        \includegraphics[width=\textwidth]{fig/dldc.pdf}
        \caption{Localization length for different cross-sections as a function of $c$ for $V = t$}
        \label{dldc}
    \end{figure}

    \begin{figure}
        \includegraphics[width=\textwidth]{fig/dldV.pdf}
        \caption{Localization length for different cross-sections as a function of $V$ for $c = 0.1$}
        \label{dldV}
    \end{figure}
\end{document}