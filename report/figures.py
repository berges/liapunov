#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import os

def gamma(no=100000, ni=50, all=False, E=0.2,
        model='Anderson', V=1.0, c=0.5, H=[[0]]):

    b = len(H)
    H = '\n'.join(' '.join(map(str, row)) for row in H)
    all = 'T' if all else 'F'

    with open('template.in') as template, \
         open('temporary.in', 'w') as temporary:
        temporary.write(template.read().format(**locals()))

    os.system('../liapunov temporary.in')

    with open('temporary.out') as temporary:
        return np.array(map(float, temporary) if all == 'F' else
            [map(float, line.split()) for line in temporary])

def loclen(gamma):
    return 0.5 / min(g for g in gamma if g > 0)

class Plot():
    def __init__(self, x='x', y='y', legend=None, width=10, height=4):
        self.figure, self.axes = plt.subplots(figsize=(width, height))

        self.axes.spines['right'].set_visible(False)
        self.axes.spines['top'].set_visible(False)

        self.axes.yaxis.set_ticks_position('left')
        self.axes.xaxis.set_ticks_position('bottom')

        plt.xlabel(x)
        plt.ylabel(y)

        self.legend = legend

        self.plot = self.axes.plot

    def save(self, name):
        plt.axis('tight')

        if self.legend:
            self.axes.legend(
                title=None if self.legend == True else self.legend,
                frameon=False)

        self.figure.savefig(name, bbox_inches='tight')

resolution = 50

file = 'fig/Agn.pdf'

if not os.path.exists(file):
    fig = Plot(r'$n$', r'$\gamma$')

    g = gamma(no=5000, all=True)

    for i in range(2):
        fig.plot(g[:, i], 'b')

    fig.save(file)

file = 'fig/AlVE.pdf'

if not os.path.exists(file):
    fig = Plot(r'$E / t$', r'$l_{\mathrm{loc.}} / a$', r'$V / t$')

    V = np.linspace(0.1, 0.5, 5)
    E = np.linspace(-2.5, 2.5, resolution)

    for v in V:
        fig.plot(E, [loclen(gamma(E=e, V=v))
            for e in E], label=r'${:.1f}$'.format(v))

    fig.save(file)

file = 'fig/AlEV.pdf'

if not os.path.exists(file):
    fig = Plot(r'$V / t$', r'$l_{\mathrm{loc.}} / a$', r'$E / t$')

    E = np.linspace(0.0, 1.5, 4)
    V = np.linspace(0.1, 0.5, resolution)

    for e in E:
        fig.plot(V, [loclen(gamma(E=e, V=v))
            for v in V], label=r'${:.1f}$'.format(e))

    fig.save(file)

file = 'fig/dlcV.pdf'

if not os.path.exists(file):
    fig = Plot(r'$V / t$', r'$l_{\mathrm{loc.}} / a$', r'$c$')

    C = np.linspace(0.01, 0.1, 4)
    V = np.linspace(0.1, 1.0, resolution)

    for c in C:
        fig.plot(V, [loclen(gamma(E=0, model='Delta', V=v, c=c))
            for v in V], label=r'${:.2f}$'.format(c))

    fig.save(file)

file = 'fig/dlVc.pdf'

if not os.path.exists(file):
    fig = Plot(r'$c$', r'$l_{\mathrm{loc.}} / a$', r'$V / t$')

    V = np.linspace(0.1, 1.0, 4)
    C = np.linspace(0.01, 0.1, resolution)

    for v in V:
        fig.plot(C, [loclen(gamma(E=0, model='Delta', V=v, c=c))
            for c in C], label=r'${:.1f}$'.format(v))

    fig.save(file)

H = [[[0]],

    [[0, 1, 1, 0 ],
    [ 1, 0, 0, 1 ],
    [ 1, 0, 0, 1 ],
    [ 0, 1, 1, 0]],

    [[0, 1, 0, 1, 0, 0, 0, 0, 0 ],
    [ 1, 0, 1, 0, 1, 0, 0, 0, 0 ],
    [ 0, 1, 0, 0, 0, 1, 0, 0, 0 ],
    [ 1, 0, 0, 0, 1, 0, 1, 0, 0 ],
    [ 0, 1, 0, 1, 0, 1, 0, 1, 0 ],
    [ 0, 0, 1, 0, 1, 0, 0, 0, 1 ],
    [ 0, 0, 0, 1, 0, 0, 0, 1, 0 ],
    [ 0, 0, 0, 0, 1, 0, 1, 0, 1 ],
    [ 0, 0, 0, 0, 0, 1, 0, 1, 0]]]

for i in 2, 3:
    file = 'fig/dg{}n.pdf'.format(i)

    if not os.path.exists(file):
        fig = Plot(r'$n$', r'$\gamma$')

        g = gamma(no=5000, ni=10, all=True, E=0.0,
            model='Delta', V=3.0, c=0.1, H=H[i - 1])

        for j in range(2 * i ** 2):
            fig.plot(g[:, j], 'b')

        fig.save(file)


file = 'fig/dldV.pdf'

if not os.path.exists(file):
    fig = Plot(r'$V / t$', r'$l_{\mathrm{loc.}} / a$', True)

    V = np.linspace(0.1, 1.0, resolution)

    for i, h in enumerate(H, 1):
        fig.plot(V, [loclen(gamma(ni=10, E=0.0, model='Delta', V=v, c=0.1, H=h))
            for v in V], label=r'${0} \times\ {0}$'.format(i))

    fig.save(file)


file = 'fig/dldc.pdf'

if not os.path.exists(file):
    fig = Plot(r'$c$', r'$l_{\mathrm{loc.}} / a$', True)

    C = np.linspace(0.01, 0.1, resolution)

    for i, h in enumerate(H, 1):
        fig.plot(C, [loclen(gamma(ni=10, E=0, model='Delta', V=1.0, c=c, H=h))
            for c in C], label=r'${0} \times\ {0}$'.format(i))

    fig.save(file)
