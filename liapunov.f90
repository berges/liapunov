program liapunov
   use command
   use global
   use io
   use random
   use solver
   implicit none

   integer :: i
   type(info) :: p

   call shuffle

   do i = 1, command_argument_count()
      call load(argument(i), p)
      call exponents(p)
      call store(p)
   end do
end program liapunov
