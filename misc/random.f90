module random
   implicit none

contains

   subroutine shuffle
      integer :: i, n, t
      integer, allocatable :: seed(:)

      call random_seed(size=n)
      allocate(seed(n))

      call system_clock(count=t)

      do i = 1, n
         seed(i) = t + 42 * i
      end do

      call random_seed(put=seed)
   end subroutine shuffle
end module random
